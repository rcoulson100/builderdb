﻿CREATE FUNCTION [kintest2008].[uf_getRate]
(
  @SORID int,
  @Quantity int
)
RETURNS DECIMAL(8,2)
AS
BEGIN
  declare @rate as decimal(8,2)

  select @rate = sum(cost)
  from
  (
    select 'cost' = case 
                      when (@Quantity > [Max]) then ([Max] - [Min] + 1) * Rate 
                      when (@Quantity >= [Min] and @Quantity <= [Max]) then (@Quantity - [Min] + 1) * Rate
                      else 0 
                    end
    from SORBands
    where SORID = @SORID
  ) as tab

  return @rate
END
