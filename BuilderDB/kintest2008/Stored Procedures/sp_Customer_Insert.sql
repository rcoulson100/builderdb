﻿
CREATE Procedure [kintest2008].sp_Customer_Insert
(
  @CustomerName nvarchar(100) = null,
  @Address1 nvarchar(100) = null,
  @Address2 nvarchar(100) = null,
  @Address3 nvarchar(100) = null,
  @Address4 nvarchar(100) = null,
  @Address5 nvarchar(100) = null,
  @PostCode nvarchar(10) = null,
  @TelNo nvarchar(20) = null,
  @CustomerID int = null output
)
--WITH ENCRYPTION
AS
Begin
  set nocount on
  
  declare @error int
  declare @rows int
  declare @save bit   
  declare @retval int
  
  set @save = 0
  set @retval = 0

  if exists (select null
             from kintest2008.Customer
             where REPLACE(CustomerName, ' ', '') = REPLACE(@CustomerName, ' ', '')
             and REPLACE(Address1, ' ', '') = REPLACE(@Address1, ' ', '')
             and REPLACE(PostCode, ' ', '') = REPLACE(@PostCode, ' ', ''))
  begin
  
    select @CustomerId = CustomerID
    from kintest2008.Customer
    where REPLACE(CustomerName, ' ', '') = REPLACE(@CustomerName, ' ', '')
    and REPLACE(Address1, ' ', '') = REPLACE(@Address1, ' ', '')
    and REPLACE(PostCode, ' ', '') = REPLACE(@PostCode, ' ', '')

    goto SP_success
  end
  
  -- Start transaction/savepoint.
  if (@@TRANCOUNT = 0)
  begin
    begin transaction sp_Customer_Insert
  end
  else
  begin
    set @save = 1
    save transaction sp_Customer_Insert
  end

  insert into kintest2008.Customer
  (
    CustomerName,
    Address1,
    Address2,
    Address3,
    Address4,
    Address5,
    PostCode,
    TelNo  
  )
  values
  (
    @CustomerName,
    @Address1,
    @Address2,
    @Address3,
    @Address4,
    @Address5,
    @PostCode,
    @TelNo  
  )

  select @CustomerID = SCOPE_IDENTITY(), @error = @@ERROR, @rows = @@ROWCOUNT
  
  if (@error <> 0)
  or (@rows != 1)
  begin
    goto SP_rollback
  end
  
  -- If didn't define a save point then this is the main transaction, so commit.
  if (@save = 0)  -- false.
    commit Transaction sp_Customer_Insert
 
  goto SP_success
 
SP_success:
  set @retval = 0  -- success.
  goto SP_return
 
SP_rollback:
  rollback transaction sp_Customer_Insert
  goto SP_failure
 
SP_not_found:
  set @retval = 50  -- not-found.
  goto SP_return
 
SP_failure:
  set @retval = -1  -- failure.
  goto SP_return
 
SP_return: 
  return @retval  

end
