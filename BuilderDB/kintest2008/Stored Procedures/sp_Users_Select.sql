﻿CREATE Procedure [kintest2008].[sp_Users_Select]
--WITH ENCRYPTION
AS
Begin

  set nocount on

  declare @error int
  declare @rows int
  
  select 'Name' = Name
       , 'Pin' = Pin
       , 'Id' = Id
  from kintest2008.Users
  order by Name
  
  select @error = @@ERROR
  if (@error <> 0)  -- not success
  begin
    return -1
  end

  return 0  
  
end
