﻿
CREATE Procedure [kintest2008].sp_WorkHeadHistory_Select
(
  @WorkId integer = null
)
--WITH ENCRYPTION
AS
Begin

  set nocount on

  declare @error int
  declare @rows int
  
  SELECT   h.WorkID
       , 'HistoryDate' = h.HistoryDate
--       , 'HistoryDate' = CONVERT(NVARCHAR, h.HistoryDate, 111) + ' '
--         + CONVERT(NVARCHAR, h.HistoryDate, 108)
       , h.NewStatusID 
       , s.StatusDesc 
       , 'StatusDate' = h.NewStatusDate 
--       , 'StatusDate' = CONVERT(NVARCHAR, h.NewStatusDate, 111) + ' '
--          + CONVERT(NVARCHAR, h.NewStatusDate, 108)
       , h.NewComments 
  FROM      kintest2008.WorkHeadHistory h
            LEFT OUTER JOIN kintest2008.statii s
                ON h.NewStatusID = s.StatusID
  WHERE h.WorkId = @WorkId
  ORDER BY h.HistoryDate
  
  select @error = @@ERROR
  if (@error <> 0)  -- not success
  begin
    return -1
  end

  return 0  
  
end
