﻿CREATE PROCEDURE [kintest2008].[sp_User_ChangePin]
(
  @Name NVARCHAR(50), 
  @OldPin INT,
  @NewPin INT
)
--WITH ENCRYPTION
AS
Begin
  set nocount on
  
  declare @rows as integer
  declare @error as integer
  declare @retval as integer

  if not exists (select null
                 from kintest2008.Users
                 where rtrim(Name) = rtrim(@Name)
                 and Pin = @OldPin)
  begin
    -- user and pin are not correct
    goto SP_failure
  end

  if (@oldPin = @NewPin)
  begin
    -- Old pin is the same as the new pin
    goto SP_failure
  end

  begin transaction

  update Users
  set Pin = @NewPin
  where Name = rtrim(@Name)

  select @rows = @@ROWCOUNT, @error = @@ERROR
  if (@rows != 1 or @error != 0)
  begin
    rollback transaction
    goto SP_failure
  end

  commit transaction

  goto SP_success
 
SP_success:
  set @retval = 0  -- success.
  goto SP_return
 
SP_not_found:
  set @retval = 50  -- not-found.
  goto SP_return
 
SP_failure:
  set @retval = -1  -- failure.
  goto SP_return
 
SP_return: 
  return @retval  

end
