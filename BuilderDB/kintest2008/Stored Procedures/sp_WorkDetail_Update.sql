﻿
CREATE Procedure [kintest2008].sp_WorkDetail_Update
(
  @WorkID integer,
  @WorkLineID integer,
  @Location nvarchar(50),
  @SurveyedUnits decimal(5,1),
  @ActualUnits decimal(5,1) = null,
  @SORID int,
  @WorkDescription nvarchar(128) = null,
  @WorkRate decimal(8,2) = null
)
--WITH ENCRYPTION
AS
Begin
  set nocount on
  
  declare @error int
  declare @rows int
  declare @save bit
  declare @retval int
  declare @rated bit

  set @save = 0
  set @retval = 0


  if not exists (select null
                 from kintest2008.WorkHead
                 where WorkID = @WorkID)
  begin
    goto SP_failure
  end

  -- Does the SOR exists and is it rated (or free text)
  select @rated = Rated
  from SOR
  where SORID = @SORID

  select @rows = @@ROWCOUNT, @error = @@ERROR
  if (@rows != 1 or @error != 0)
    goto SP_failure
  
  -- if not rated we need to make sure the work description and rate is passed in
  if (@rated = 0)
  begin
    if (@WorkDescription is null or rtrim(@WorkDescription ) = '')
    begin
      goto SP_failure
    end 

    if (@WorkRate is null)
    begin
      goto SP_failure
    end 
  end
  else
  begin
    set @WorkDescription = null
    set @WorkRate = null
  end

  -- Start transaction/savepoint.
  if (@@TRANCOUNT = 0)
  begin
    begin transaction sp_WorkDetail_Update
  end
  else
  begin
    set @save = 1
    save transaction sp_WorkDetail_Update
  end

  UPDATE WorkDetail 
  SET Location = @Location
    , SurveyedUnits = @SurveyedUnits
    , ActualUnits = @ActualUnits
    , SORID = @SORID
    , WorkDescription = @WorkDescription
    , WorkRate = @WorkRate
  WHERE WorkID = @WorkID
  AND WorkLineID = @WorkLineID

  select @error = @@ERROR, @rows = @@ROWCOUNT
  
  if (@error <> 0)
  or (@rows != 1)
  begin
    goto SP_rollback
  end

  -- If didn't define a save point then this is the main transaction, so commit.
  if (@save = 0)  -- false.
    commit Transaction sp_WorkDetail_Update
 
  goto SP_success
 
SP_success:
  set @retval = 0  -- success.
  goto SP_return
 
SP_rollback:
  rollback transaction sp_WorkDetail_Update
  goto SP_failure
 
SP_not_found:
  set @retval = 50  -- not-found.
  goto SP_return
 
SP_failure:
  set @retval = -1  -- failure.
  goto SP_return
 
SP_return: 
  return @retval  

end
