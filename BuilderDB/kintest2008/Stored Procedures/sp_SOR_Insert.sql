﻿CREATE Procedure [kintest2008].[sp_SOR_Insert]
(
  @SORDesc nvarchar(200),
  @Rated bit
)
--WITH ENCRYPTION
AS
Begin
  set nocount on
  
  declare @error int
  declare @rows int
  declare @save bit   
  declare @retval int
  
  set @save = 0
  set @retval = 0

  -- Start transaction/savepoint.
  if (@@TRANCOUNT = 0)
  begin
    begin transaction sp_SOR_Insert
  end
  else
  begin
    set @save = 1
    save transaction sp_SOR_Insert
  end

  insert into SOR (SORDesc, Display, Rated)
  values (@SORDesc, 1, @Rated)

  select @error = @@ERROR, @rows = @@ROWCOUNT 
  if (@error <> 0)
  or (@rows != 1)
  begin
    goto SP_rollback
  end

  -- If didn't define a save point then this is the main transaction, so commit.
  if (@save = 0)  -- false.
    commit Transaction sp_SOR_Insert
 
  goto SP_success
 
SP_success:
  set @retval = 0  -- success.
  goto SP_return
 
SP_rollback:
  rollback transaction sp_SOR_Insert
  goto SP_failure
 
SP_not_found:
  set @retval = 50  -- not-found.
  goto SP_return
 
SP_failure:
  set @retval = -1  -- failure.
  goto SP_return
 
SP_return: 
  return @retval  

end

