﻿CREATE PROCEDURE [dbo].[sp_RepCompletedJobs_Select]
(
  @startDate datetime = null,
  @endDate datetime = null
)
--WITH ENCRYPTION
AS
Begin

  set nocount on

  declare @error int
  declare @rows int
  declare @workid int
  declare @jobnumber nvarchar(20)
  declare @address nvarchar(40)
  declare @actualtotal decimal(7,2)
  declare @row int
  declare @sor nvarchar(10)
  declare @units int
  declare @retval int
  
  IF OBJECT_ID (N'tempdb..#RepCompleted') IS NOT NULL 
    drop table #RepCompleted

  select tab.*, h.JobNumber, rtrim(c.address1) + ' ' + rtrim(Postcode) as 'address'
  into #RepCompleted
  from
  (
    select 'WorkID' = d.WorkID
         , 'ShortSOR' = left(ltrim(sor.SORDesc), charindex(' -', ltrim(sor.SORDesc)))
         , 'SOR' = rtrim(sor.SORDesc)
         , 'SurveyedUnits' = cast(sum(d.SurveyedUnits) as int)
         , 'ActualUnits' = cast(sum(d.ActualUnits) as int)
         , 'SurveyedTotal' = kintest2008.uf_getRate(d.SORID, sum(d.SurveyedUnits))
         , 'ActualTotal' = kintest2008.uf_getRate(d.SORID, sum(d.ActualUnits))
    from kintest2008.WorkDetail d
      left outer join kintest2008.SOR sor
        on sor.SORID = d.SORID
    where sor.rated = 1
    group by d.SORID, d.WorkID
         , sor.SORDesc
         , sor.rated
  ) as tab
    inner join kintest2008.WorkHead h
      on h.workid = tab.workid
    inner join kintest2008.customer c
      on h.customerid = c.customerid
  where (@startDate is null or h.DateCompleted >= @startDate)
  and (@endDate is null or h.DateCompleted <= @endDate )
  and h.StatusID = 99
  order by tab.workid

  --select @error = @@ERROR, @rows = @@ROWCOUNT
  --if (@error != 0 or @rows = 0)
  --  goto SP_error


  IF OBJECT_ID (N'tempdb..#RepCompletedRes') IS NOT NULL 
    drop table #RepCompletedRes

  create table #RepCompletedRes 
  (
    workid int,
    jobnumber nvarchar(20),
    sor1 nvarchar(10),
    unit1 int,
    sor2 nvarchar(10),
    unit2 int,
    sor3 nvarchar(10),
    unit3 int,
    sor4 nvarchar(10),
    unit4 int,
    sor5 nvarchar(10),
    unit5 int,
    sor6 nvarchar(10),
    unit6 int,
    sor7 nvarchar(10),
    unit7 int,
    sor8 nvarchar(10),
    unit8 int,
    sor9 nvarchar(10),
    unit9 int,
    sor10 nvarchar(10),
    unit10 int,
    address  nvarchar(40),
    actualtotal decimal(7,2),
    vat  decimal(7,2),
    invoicetotal  decimal(7,2),
    comments nvarchar(200)
  )


  declare cr_rep_completed cursor for
  select workid, jobnumber, address, actualtotal=sum(actualtotal)
  from #RepCompleted 
  group by workid, jobnumber, address
  order by workid

  open cr_rep_completed

  fetch next 
  from cr_rep_completed
  into @workid, @jobnumber, @address, @actualtotal

  while (@@fetch_status = 0)
  begin

    insert into #RepCompletedRes (workid, jobnumber, address, actualtotal)
    values (@workid, @jobnumber, @address, @actualtotal)

    declare cr_rep_completed_inner cursor for
    SELECT ROW_NUMBER() OVER(ORDER BY ShortSOR) AS Row, ShortSOR, ActualUnits 
    FROM #RepCompleted 
    where workid = @workid

    open cr_rep_completed_inner

    fetch next 
    from cr_rep_completed_inner
    into @row, @sor, @units

    while (@@fetch_status = 0)
    begin
      if (@row = 1)
      begin
        update #RepCompletedRes
        set sor1 = @sor
        , unit1 = @units
        where workid = @workid
      end
      else if (@row = 2)
      begin
        update #RepCompletedRes
        set sor2 = @sor
        , unit2 = @units
        where workid = @workid
      end
      else if (@row = 3)
      begin
        update #RepCompletedRes
        set sor3 = @sor
        , unit3 = @units
        where workid = @workid
      end
      else if (@row = 4)
      begin
        update #RepCompletedRes
        set sor4 = @sor
        , unit4 = @units
        where workid = @workid
      end
      else if (@row = 5)
      begin
        update #RepCompletedRes
        set sor5 = @sor
        , unit5 = @units
        where workid = @workid
      end
      else if (@row = 6)
      begin
        update #RepCompletedRes
        set sor6 = @sor
        , unit6 = @units
        where workid = @workid
      end
      else if (@row = 7)
      begin
        update #RepCompletedRes
        set sor7 = @sor
        , unit7 = @units
        where workid = @workid
      end
      else if (@row = 8)
      begin
        update #RepCompletedRes
        set sor8 = @sor
        , unit8 = @units
        where workid = @workid
      end
      else if (@row = 9)
      begin
        update #RepCompletedRes
        set sor9 = @sor
        , unit9 = @units
        where workid = @workid
      end
      else if (@row = 10)
      begin
        update #RepCompletedRes
        set sor10 = @sor
        , unit10 = @units
        where workid = @workid
      end

      fetch next 
      from cr_rep_completed_inner
      into @row, @sor, @units

    end
    close cr_rep_completed_inner
    deallocate cr_rep_completed_inner
    
    fetch next 
    from cr_rep_completed
    into @workid, @jobnumber, @address, @actualtotal

  end
  close cr_rep_completed
  deallocate cr_rep_completed


  select 
    'Job Number' = jobnumber,
    'Address' = address,
    'SOR 1' = sor1,
    'Quantity 1' = unit1,
    'SOR 2' = sor2 ,
    'Quantity 2' = unit2 ,
    'SOR 3' = sor3 ,
    'Quantity 3' = unit3 ,
    'SOR 4' = sor4 ,
    'Quantity 4' = unit4 ,
    'SOR 5' = sor5 ,
    'Quantity 5' = unit5 ,
    'SOR 6' = sor6 ,
    'Quantity 6' = unit6 ,
    'SOR 7' = sor7 ,
    'Quantity 7' = unit7 ,
    'SOR 8' = sor8 ,
    'Quantity 8' = unit8 ,
    'SOR 9' = sor9 ,
    'Quantity 9' = unit9 ,
    'SOR 10' = sor10 ,
    'Quantity 10' = unit10 ,
    'Total' = actualtotal,
    'VAT' = vat,
    'Invoice Total' = invoicetotal,
    comments
  from #RepCompletedRes

  select @error = @@ERROR, @rows = @@ROWCOUNT
  if (@error != 0 or @rows = 0)
    goto SP_error

SP_success:
  set @retval = 0
  goto SP_return

SP_error:
  set @retval = -1
  goto SP_return

SP_return:
  IF CURSOR_STATUS('global','cr_rep_completed') = 1
    close cr_rep_completed
  IF CURSOR_STATUS('global','cr_rep_completed') = -1
    deallocate cr_rep_completed

  IF CURSOR_STATUS('global','cr_rep_completed_inner') = 1
    close cr_rep_completed_inner
  IF CURSOR_STATUS('global','cr_rep_completed_inner') = -1
    deallocate cr_rep_completed_inner

  IF OBJECT_ID (N'tempdb..#RepCompleted') IS NOT NULL 
    drop table #RepCompleted
  IF OBJECT_ID (N'tempdb..#RepCompletedRes') IS NOT NULL 
    drop table #RepCompletedRes

  return @retval
  
end

