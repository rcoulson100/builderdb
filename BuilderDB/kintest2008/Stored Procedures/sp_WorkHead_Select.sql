﻿
CREATE Procedure [kintest2008].sp_WorkHead_Select
(
  @WorkId integer = null,
  @JobNumber integer = null,
  @AllOpen bit = null
)
--WITH ENCRYPTION
AS
Begin

  set nocount on

  declare @error int
  declare @rows int
  
  -- WorkId takes priority
  if (@WorkId is not null) and (@JobNumber is not null)
  begin
    set @JobNumber = null
  end

  SELECT    h.WorkID
          , h.JobNumber
          , 'DateRaised' = h.DateRaised
          --, 'DateRaised' = CONVERT(NVARCHAR, h.DateRaised, 111) + ' '
          --  + CONVERT(NVARCHAR, h.DateRaised, 108)
          , 'DateIssued' = h.DateIssued
          --, 'DateIssued' = CONVERT(NVARCHAR, h.DateIssued, 111) + ' '
          --  + CONVERT(NVARCHAR, h.DateIssued, 108)
          , 'ClerkName' = cl.Name 
          , h.ClerkID
          , p.PriorityDesc
          , s.StatusDesc
          , h.StatusID
          , 'StatusDate' = h.StatusDate  
          --, 'StatusDate' = CONVERT(NVARCHAR, h.StatusDate, 111) + ' '
          --  + CONVERT(NVARCHAR, h.StatusDate, 108)
          , h.Comments 
          , h.DateOnSite 
          , h.DateCompleted
          , h.CustomerID
          , c.CustomerName
          , c.Address1
          , c.Address2
          , c.Address3
          , c.Address4
          , c.Address5
          , c.PostCode
          , c.TelNo
          /*
          , 'SurveyedUnitTotal' = 
            (
              SELECT sum( 
                case 
                  when s.Rated = 1
                  then kintest2008.uf_getRate(wd.SORID, wd.SurveyedUnits)
                  else wd.WorkRate
                end
                )
              FROM kintest2008.WorkDetail wd
              INNER JOIN kintest2008.SOR s ON s.SORID = wd.SORID
              WHERE wd.WorkID = h.WorkID
            )
          , 'ActualUnitTotal' = 
            (
              SELECT sum( 
                case 
                  when s.Rated = 1
                  then kintest2008.uf_getRate(wd.SORID, wd.ActualUnits)
                  else wd.WorkRate
                end
                )
              FROM kintest2008.WorkDetail wd
              INNER JOIN kintest2008.SOR s ON s.SORID = wd.SORID
              WHERE wd.WorkID = h.WorkID
            )
            */
          , 'SurveyedUnitTotal' = 
            (
              select sum(t.surveyedtotal)
              from
              (
                select 'SurveyedTotal' = kintest2008.uf_getRate(d.SORID, sum(d.SurveyedUnits))
                from kintest2008.WorkDetail d
                left outer join kintest2008.SOR sor
                  on sor.SORID = d.SORID
                where sor.rated = 1
                and d.workid = h.WorkID
                group by d.SORID, d.WorkID
                  , sor.SORDesc
                  , sor.rated
                /*
                union

                select 'SurveyedTotal' = d.WorkRate 
                from kintest2008.WorkDetail d
                  left outer join kintest2008.SOR sor
                    on sor.SORID = d.SORID
                where sor.rated = 0
                and workid = 3
                */
              ) t
            )
          , 'ActualUnitTotal' = 
            (
              select sum(t.ActualTotal)
              from
              (
                select 'ActualTotal' = kintest2008.uf_getRate(d.SORID, sum(d.ActualUnits))
                from kintest2008.WorkDetail d
                left outer join kintest2008.SOR sor
                  on sor.SORID = d.SORID
                where sor.rated = 1
                and d.workid = h.WorkID
                group by d.SORID
                  , d.WorkID
                  , sor.SORDesc
                  , sor.rated
                /*
                union

                select 'ActualTotal' = d.WorkRate 
                from kintest2008.WorkDetail d
                  left outer join kintest2008.SOR sor
                    on sor.SORID = d.SORID
                where sor.rated = 0
                and workid = 3
                */
              ) t
            )
  FROM      kintest2008.WorkHead h
            LEFT OUTER JOIN kintest2008.Customer c
                ON c.CustomerID = h.CustomerID
            LEFT OUTER JOIN kintest2008.statii s
                ON h.StatusID = s.StatusID
            LEFT OUTER JOIN kintest2008.priority p
                ON h.Priority = p.Priority
            LEFT OUTER JOIN Clerks cl
                ON cl.ClerkID = h.ClerkID
  WHERE     (
              h.WorkId = @WorkId
              OR @WorkId IS NULL
            )
            AND (
                  h.JobNumber = @JobNumber
                  OR @JobNumber IS NULL
                )
            AND (
                  @AllOpen IS NULL
                  OR @AllOpen = 0
                  OR (
                       @AllOpen = 1
                       AND (
                             h.StatusID < 99
                             OR (
                                  h.StatusID = 99
                                  AND h.StatusDate >= DATEADD(mm, -3, GETDATE())
                                )
                           )
                     )
                )

  
  select @error = @@ERROR
  if (@error <> 0)  -- not success
  begin
    return -1
  end

  return 0  
  
end
