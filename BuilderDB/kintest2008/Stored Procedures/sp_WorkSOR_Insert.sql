﻿-- redundant - can delete after publish
CREATE Procedure [kintest2008].sp_WorkSOR_Insert
(
  @WorkID integer,  
  @WorkLineID integer,
  @SORID integer
)
--WITH ENCRYPTION
AS
Begin
  set nocount on
  
  declare @error int
  declare @rows int
  declare @save bit   
  declare @retval int
  
  set @save = 0
  set @retval = 0


  if not exists (select null
                 from kintest2008.WorkDetail
                 where WorkID = @WorkID
                 and WorkLineID = @WorkLineID)
  begin
    goto SP_failure
  end

  if not exists (select null
                 from kintest2008.SOR
                 where SORID = @SORID)
  begin
    goto SP_failure
  end
  
  -- Start transaction/savepoint.
  if (@@TRANCOUNT = 0)
  begin
    begin transaction sp_WorkSOR_Insert
  end
  else
  begin
    set @save = 1
    save transaction sp_WorkSOR_Insert
  end

  insert into kintest2008.WorkSOR
  (
    WorkID,
    WorkLineID,
    SORID
  )
  values
  (
    @WorkID,
    @WorkLineID,
    @SORID
  )

  select @error = @@ERROR, @rows = @@ROWCOUNT
  
  if (@error <> 0)
  or (@rows != 1)
  begin
    goto SP_rollback
  end
 
  -- If didn't define a save point then this is the main transaction, so commit.
  if (@save = 0)  -- false.
    commit Transaction sp_WorkSOR_Insert
 
  goto SP_success
 
SP_success:
  set @retval = 0  -- success.
  goto SP_return
 
SP_rollback:
  rollback transaction sp_WorkSOR_Insert
  goto SP_failure
 
SP_not_found:
  set @retval = 50  -- not-found.
  goto SP_return
 
SP_failure:
  set @retval = -1  -- failure.
  goto SP_return
 
SP_return: 
  return @retval  

end
