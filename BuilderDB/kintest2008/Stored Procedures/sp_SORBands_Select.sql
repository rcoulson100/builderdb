﻿CREATE Procedure [kintest2008].[sp_SORBands_Select]
(
  @SorID int
)
--WITH ENCRYPTION
AS
Begin

  set nocount on

  declare @error int
  declare @rows int

  select SORID
       , BandID
       , 'Band' = rtrim(Band)
       , 'Min' = cast([Min] as int)
       , 'Max' = cast([Max] as int)
       , Rate 
  from kintest2008.SORBands
  where SORID = @SorID
  order by [Min], [Max]
  
  select @error = @@ERROR
  if (@error <> 0)  -- not success
  begin
    return -1
  end

  return 0  
  
end

