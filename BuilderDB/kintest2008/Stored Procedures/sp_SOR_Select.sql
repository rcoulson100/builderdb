﻿CREATE Procedure [kintest2008].[sp_SOR_Select]
--WITH ENCRYPTION
AS
Begin

  set nocount on

  declare @error int
  declare @rows int

  select SORID
       , SORDesc
       , 'RatedText' = case when Rated = 0 then 'No' else 'Yes' end
       , Rated
  from kintest2008.SOR
  where Display = 1
  order by SORDesc
  
  select @error = @@ERROR
  if (@error <> 0)  -- not success
  begin
    return -1
  end

  return 0  
  
end

