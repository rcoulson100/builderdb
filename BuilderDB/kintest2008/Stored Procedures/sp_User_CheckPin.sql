﻿CREATE PROCEDURE [kintest2008].[sp_User_CheckPin]
(
  @Name NVARCHAR(50), 
  @Pin INT
)
--WITH ENCRYPTION
AS
Begin
  set nocount on
  
  declare @retval as integer

  if not exists (select null
                 from kintest2008.Users
                 where rtrim(Name) = rtrim(@Name)
                 and Pin = @Pin)
  begin
    -- user and pin are not correct
    goto SP_not_found
  end

  goto SP_success
 
SP_success:
  set @retval = 0  -- success.
  goto SP_return
 
SP_not_found:
  set @retval = 50  -- not-found.
  goto SP_return
 
SP_failure:
  set @retval = -1  -- failure.
  goto SP_return
 
SP_return: 
  return @retval  

end
