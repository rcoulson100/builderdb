﻿CREATE PROCEDURE [kintest2008].[sp_WorkHead_Validate]
(
  @JobNumber integer = null,
  @WorkId integer = null
)
--WITH ENCRYPTION
AS
Begin
  set nocount on
  
  declare @retval int
  
  set @retval = 0

  if exists (select null
             from kintest2008.WorkHead
             where JobNumber = @JobNumber
             and (@WorkId is null or WorkID != @WorkId))
  begin
    set @retval = -1  -- job number already in use
    goto SP_return
  end
 
  goto SP_success
 
SP_success:
  set @retval = 0  -- success.
  goto SP_return
   
SP_return: 
  return @retval  

end
