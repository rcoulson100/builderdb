﻿CREATE PROCEDURE [kintest2008].[sp_Listbox_Locations_Select]
--WITH ENCRYPTION
AS
Begin
  set nocount on
  
  declare @retval as integer

  SELECT LocationDesc
       , LocationID
  FROM Location
  WHERE Display = 1
  ORDER BY LocationDesc

  goto SP_success
 
SP_success:
  set @retval = 0  -- success.
  goto SP_return
 
SP_not_found:
  set @retval = 50  -- not-found.
  goto SP_return
 
SP_failure:
  set @retval = -1  -- failure.
  goto SP_return
 
SP_return: 
  return @retval  

end
