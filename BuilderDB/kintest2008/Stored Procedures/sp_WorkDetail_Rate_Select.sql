﻿CREATE Procedure [kintest2008].[sp_WorkDetail_Rate_Select]
(
  @WorkId integer = null
)
--WITH ENCRYPTION
AS
Begin

  set nocount on

  declare @error int
  declare @rows int

  -- select into temp tables so we can do a totals row
  select *
  into #results
  from
  (
    select 'WorkID' = d.WorkID
         , 'SOR' = rtrim(sor.SORDesc)
         , 'SurveyedUnits' = cast(sum(d.SurveyedUnits) as int)
         , 'ActualUnits' = cast(sum(d.ActualUnits) as int)
         , 'SurveyedTotal' = kintest2008.uf_getRate(d.SORID, sum(d.SurveyedUnits))
         , 'ActualTotal' = kintest2008.uf_getRate(d.SORID, sum(d.ActualUnits))
         , 'WorkDescription' = null
         , 'WorkRate' = null
         , 'OrderField' = 1
    from kintest2008.WorkDetail d
      left outer join kintest2008.SOR sor
        on sor.SORID = d.SORID
    where sor.rated = 1
    and workid = @WorkId
    group by d.SORID, d.WorkID
         , sor.SORDesc
         , sor.rated

    union all

    select 'WorkID' = d.WorkID
         , 'SOR' = sor.SORDesc
         , 'SurveyedUnits' = cast(d.SurveyedUnits as int)
         , 'ActualUnits' = cast(d.ActualUnits as int)
         , 'SurveyedTotal' = null
         , 'ActualTotal' = null
         , 'WorkDescription' = rtrim(d.WorkDescription)
         , 'WorkRate' = d.WorkRate 
         , 'OrderField' = 1
    from kintest2008.WorkDetail d
      left outer join kintest2008.SOR sor
        on sor.SORID = d.SORID
    where sor.rated = 0
    and workid = @WorkId
  ) tab

  select @error = @@ERROR
  if (@error <> 0)  -- not success
  begin
    return -1
  end

  -- get deails from temp table with a totals record
  select WorkID
       , SOR
       , SurveyedUnits
       , ActualUnits
       , SurveyedTotal
       , ActualTotal
       , WorkDescription
       , WorkRate
  from
  (
    select *
    from #results
    union all
    select null
         , 'Totals'
         , null
         , null
         , sum(SurveyedTotal)
         , sum(ActualTotal)
         , null
         , sum(WorkRate)
         , 99
    from #results
  ) tab
  order by OrderField
         , SOR

  return 0  
  
end
