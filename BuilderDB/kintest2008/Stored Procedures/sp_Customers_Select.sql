﻿CREATE Procedure [kintest2008].[sp_Customers_Select]
--WITH ENCRYPTION
AS
Begin

  set nocount on

  declare @error int
  declare @rows int
  
  select CustomerID
       , CustomerName
       , Address1
       , Address2
       , Address3
       , Address4
       , Address5
       , PostCode
       , TelNo
  from kintest2008.Customer
  order by PostCode
  
  select @error = @@ERROR
  if (@error <> 0)  -- not success
  begin
    return -1
  end

  return 0  
  
end
