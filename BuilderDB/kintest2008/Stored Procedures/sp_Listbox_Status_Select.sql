﻿CREATE PROCEDURE [kintest2008].[sp_Listbox_Status_Select]
--WITH ENCRYPTION
AS
Begin
  set nocount on
  
  declare @retval as integer

  SELECT StatusDesc
       , StatusID
  FROM Statii 
  ORDER BY StatusID

  goto SP_success
 
SP_success:
  set @retval = 0  -- success.
  goto SP_return
 
SP_not_found:
  set @retval = 50  -- not-found.
  goto SP_return
 
SP_failure:
  set @retval = -1  -- failure.
  goto SP_return
 
SP_return: 
  return @retval  

end
