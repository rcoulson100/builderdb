﻿
CREATE Procedure [kintest2008].sp_WorkDetail_Select
(
  @WorkId integer = null,
  @WorkLineId integer = null
)
--WITH ENCRYPTION
AS
Begin

  set nocount on

  declare @error int
  declare @rows int
  
  select 'WorkID' = d.WorkID
       , 'WorkLineID' = d.WorkLineID
       , 'Location' = d.Location
       , 'SOR' = rtrim(sor.SORDesc)
       , 'SurveyedUnits' = cast(d.SurveyedUnits as int)
       , 'ActualUnits' = cast(d.ActualUnits as int)
/*       , 'SurveyedTotal' = 
          case 
            when sor.Rated = 1 
            then kintest2008.uf_getRate(d.SORID, d.SurveyedUnits)
            else null
          end
       , 'ActualTotal' = 
          case 
            when sor.Rated = 1 
            then kintest2008.uf_getRate(d.SORID, d.ActualUnits)
            else null
          end*/
       , 'WorkDescription' = 
          case 
            when sor.Rated = 0 
            then rtrim(d.WorkDescription )
            else null
          end
       , 'WorkRate' =
          case 
            when sor.Rated = 0
            then d.WorkRate
            else 0
          end
       , 'Rated' = case when sor.Rated = 1 then 1 else 0 end 
  from kintest2008.WorkDetail d
    left outer join kintest2008.SOR sor
      on sor.SORID = d.SORID
  where d.WorkId = @WorkId
  and (d.WorkLineID = @WorkLineId or @WorkLineId is null)
  
  select @error = @@ERROR
  if (@error <> 0)  -- not success
  begin
    return -1
  end

  return 0  
  
end
