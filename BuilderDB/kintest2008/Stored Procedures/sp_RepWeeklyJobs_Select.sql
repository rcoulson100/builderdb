﻿CREATE PROCEDURE [dbo].[sp_RepWeeklyJobs_Select]
(
  @startDate datetime = null,
  @endDate datetime = null
)
--WITH ENCRYPTION
AS
Begin

  set nocount on

  declare @error int
  declare @rows int
  

  SELECT    'LCC Works Order' = h.JobNumber
          , 'Technician' = cl.Name 
          , 'Address' = rtrim(c.Address1) + ', ' + rtrim(c.Address2)+ ', ' + rtrim(c.Address3) + ', ' + rtrim(c.PostCode)
          , 'Name Of Tenant' = c.CustomerName
          , 'Tel No.' = c.TelNo 
          , 'Priority' = p.PriorityDesc
          , 'Date Received' = CONVERT(NVARCHAR, h.DateRaised, 111) + ' '
            + CONVERT(NVARCHAR, h.DateRaised, 108)
          , 'Status' = s.StatusDesc
          , 'Reason' = h.Comments
          , 'Start Date' =  CONVERT(NVARCHAR, h.DateOnSite, 111) + ' '
            + CONVERT(NVARCHAR, h.DateOnSite, 108)
          , 'Completed Date' =  CONVERT(NVARCHAR, h.DateCompleted, 111) + ' '
            + CONVERT(NVARCHAR, h.DateCompleted, 108)
  FROM      kintest2008.WorkHead h
            LEFT OUTER JOIN kintest2008.Customer c
                ON c.CustomerID = h.CustomerID
            LEFT OUTER JOIN kintest2008.statii s
                ON h.StatusID = s.StatusID
            LEFT OUTER JOIN kintest2008.priority p
                ON h.Priority = p.Priority
            LEFT OUTER JOIN kintest2008.Clerks cl
                ON cl.ClerkID = h.ClerkID
  WHERE h.DateRaised between @startDate and @endDate 
  order by h.DateRaised 
  
  select @error = @@ERROR
  if (@error <> 0)  -- not success
  begin
    return -1
  end

  return 0  
  
end

