﻿CREATE TRIGGER [tr_workHeadHistory]
  ON [kintest2008].[WorkHead]
  FOR INSERT, UPDATE
  AS
  BEGIN
    SET NOCOUNT ON

    insert into WorkHeadHistory 
    (
      HistoryDate,
      WorkID,
      NewJobNumber,
      NewDateRaised,
      NewDateIssued,
      NewRaisedBy,
      NewClerkID,
      NewPriority,
      NewStatusID,
      NewStatusDate,
      NewCustomerID,
      NewComments,
      OldJobNumber,
      OldDateRaised,
      OldDateIssued,
      OldRaisedBy,
      OldClerkID,
      OldPriority,
      OldStatusID,
      OldStatusDate,
      OldCustomerID,
      OldComments
    )
    select getdate(),
            coalesce(i.workid, d.WorkId),
            i.JobNumber,
            i.DateRaised,
            i.DateIssued,
            i.RaisedBy,
            i.ClerkID,
            i.Priority,
            i.StatusID,
            i.StatusDate,
            i.CustomerID,
            i.Comments,
            d.JobNumber,
            d.DateRaised,
            d.DateIssued,
            d.RaisedBy,
            d.ClerkID,
            d.Priority,
            d.StatusID,
            d.StatusDate,
            d.CustomerID,
            d.Comments
    from inserted i
      full outer join deleted d
        on i.WorkID = d.WorkID

    return

  END
  