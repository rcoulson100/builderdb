﻿CREATE TABLE [kintest2008].[Priority]
(
    [Priority] INT NOT NULL PRIMARY KEY, 
    [PriorityDesc] NVARCHAR(50) NOT NULL, 
    [Defflag] BIT NULL
)
