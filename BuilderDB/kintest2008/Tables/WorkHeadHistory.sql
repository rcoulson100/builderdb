﻿CREATE TABLE [kintest2008].[WorkHeadHistory] (
    [HistoryDate] DATETIME       NOT NULL,
    [WorkID]      INT            NOT NULL,
    [NewJobNumber]   INT            NULL,
    [NewDateRaised]  DATETIME       NULL,
    [NewDateIssued]  DATETIME       NULL,
    [NewRaisedBy]    NVARCHAR (100) NULL,
    [NewClerkID]       INT NULL,
    [NewPriority]    INT            NULL,
    [NewStatusID]      INT            NULL,
    [NewStatusDate]   DATETIME       NULL,
    [NewCustomerID]  INT            NULL,
    [NewComments]   NVARCHAR(256)          NULL,
    [OldJobNumber]  INT            NULL,
    [OldDateRaised] DATETIME       NULL,
    [OldDateIssued] DATETIME       NULL,
    [OldRaisedBy]   NVARCHAR (100) NULL,
    [OldClerkID]    INT         NULL,
    [OldPriority]   INT            NULL,
    [OldStatusID]   INT            NULL,
    [OldStatusDate] DATETIME       NULL,
    [OldCustomerID] INT            NULL,
    [OldComments]   NVARCHAR(256)          NULL,
    CONSTRAINT [FK_WorkHeadHistory_WorkHead] FOREIGN KEY (WorkID) REFERENCES kintest2008.WorkHead (WorkID) ON DELETE CASCADE
);


GO

CREATE INDEX [IX_WorkHeadHistory_Column] ON [kintest2008].[WorkHeadHistory] (WorkID)
