﻿CREATE TABLE [kintest2008].[Clerks]
(
  [ClerkId] INT NOT NULL PRIMARY KEY,
  [Name] NVARCHAR(50) NOT NULL, 
  [Display] BIT NOT NULL
)
