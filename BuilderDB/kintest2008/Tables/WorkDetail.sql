﻿CREATE TABLE [kintest2008].[WorkDetail] (
    [WorkID]        INT NOT NULL,
    [WorkLineID]    INT IDENTITY (1, 1) NOT NULL,
    [SurveyedUnits] DECIMAL(5, 2) NULL,
    [ActualUnits]   DECIMAL(5, 2) NULL, 
    [Location] NVARCHAR(50) NULL, 
    [SORID] INT NOT NULL, 
    [WorkDescription] NCHAR(128) NULL, 
    [WorkRate] DECIMAL(8, 2) NULL, 
    CONSTRAINT [PK_WorkDetail] PRIMARY KEY ([WorkID], [WorkLineID]), 
    CONSTRAINT [FK_WorkDetail_WorkHead] FOREIGN KEY (WorkID) REFERENCES kintest2008.WorkHead (WorkID) ON DELETE CASCADE
);

