﻿CREATE TABLE [kintest2008].[SOR] (
    [SORID]   INT            IDENTITY (1, 1) NOT NULL,
    [SORDesc] NVARCHAR (200)  NOT NULL,
    [Display] BIT NOT NULL, 
    [Rated] BIT NULL, 
    CONSTRAINT [PK_SOR] PRIMARY KEY ([SORID])
);

