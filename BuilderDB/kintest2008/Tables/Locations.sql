﻿CREATE TABLE [kintest2008].[Location]
(
    [LocationId] INT IDENTITY (1, 1) NOT NULL PRIMARY KEY, 
    [LocationDesc] NVARCHAR(50) NOT NULL, 
    [Display] BIT NOT NULL
)
