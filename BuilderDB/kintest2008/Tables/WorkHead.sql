﻿CREATE TABLE [kintest2008].[WorkHead] (
    [WorkID]     INT            IDENTITY (1, 1) NOT NULL,
    [JobNumber]  INT            NOT NULL,
    [DateRaised] DATETIME       NULL,
    [DateIssued] DATETIME       NOT NULL,
    [RaisedBy]   NVARCHAR (100) NULL,
    [ClerkID]    INT            NULL,
    [Priority]   INT            NOT NULL,
    [StatusID]   INT            NOT NULL,
    [StatusDate] DATETIME       NOT NULL,
    [CustomerID] INT            NOT NULL,
    [Comments]   NVARCHAR(256)  NULL, 
    [DateOnSite] DATETIME       NULL,
    [DateCompleted] DATETIME    NULL,
    CONSTRAINT [pk_WorkHead] PRIMARY KEY CLUSTERED ([WorkID] ASC)
);

