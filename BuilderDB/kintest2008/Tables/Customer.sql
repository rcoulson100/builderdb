﻿CREATE TABLE [kintest2008].[Customer] (
    [CustomerID]   INT            IDENTITY (1, 1) NOT NULL,
    [CustomerName] NVARCHAR (100) NULL,
    [Address1]     NVARCHAR (100) NULL,
    [Address2]     NVARCHAR (100) NULL,
    [Address3]     NVARCHAR (100) NULL,
    [Address4]     NVARCHAR (100) NULL,
    [Address5]     NVARCHAR (100) NULL,
    [PostCode]     NVARCHAR (10)  NULL,
    [TelNo]        NVARCHAR (20)  NULL, 
    CONSTRAINT [PK_Customer] PRIMARY KEY (CustomerID)
);

